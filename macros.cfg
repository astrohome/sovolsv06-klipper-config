[gcode_macro LOAD_FILAMENT]
gcode:
  SAVE_GCODE_STATE NAME=load_state
  G91
# Heat up hotend to provided temp or 220 as default as that should work OK with most filaments.
  {% if params.TEMP is defined or printer.extruder.can_extrude|lower == 'false' %}
    M117 Heating...
    M104 S{params.TEMP|default(220, true)}
    TEMPERATURE_WAIT SENSOR=extruder MINIMUM={params.TEMP|default(220, true)}
  {% endif %}
  M117 Loading filament...
  # Load the filament into the hotend area.
  G0 E65 F400
  # Wait a secod
  G4 P1000
  # Purge
  G0 E40 F100
  # Wait for purge to complete
  M400e
  M117 Filament loaded!
  RESTORE_GCODE_STATE NAME=load_state

[gcode_macro UNLOAD_FILAMENT]
gcode:
  SAVE_GCODE_STATE NAME=unload_state
  G91
  {% if params.TEMP is defined or printer.extruder.can_extrude|lower == 'false' %}
    M117 Heating...
    # Heat up hotend to provided temp or 220 as default as that should work OK with most filaments.
    M104 S{params.TEMP|default(220, true)}
    TEMPERATURE_WAIT SENSOR=extruder MINIMUM={params.TEMP|default(220, true)}
  {% endif %}
  M117 Unloading filament...
  # Extract filament to cold end area
  G0 E-5 F3000
  # Wait for three seconds
  G4 P3000
  # Push back the filament to smash any stringing
  G0 E5 F3000
  # Extract back fast in to the cold zone
  G0 E-15 F3000
  # Continue extraction slowly, allow the filament time to cool solid before it reaches the gears
  G0 E-60 F300
  M117 Filament unloaded!
  RESTORE_GCODE_STATE NAME=unload_state


[gcode_macro M600] #Filament change
gcode:
    {% set X = params.X|default(50)|float %}
    {% set Y = params.Y|default(0)|float %}
    {% set Z = params.Z|default(10)|float %}
    SAVE_GCODE_STATE NAME=M600_state
    PAUSE
    G91
    G1 E-.8 F2700
    G1 Z{Z}
    G90
    G1 X{X} Y{Y} F3000
    G91
    G1 E-50 F1000
    RESTORE_GCODE_STATE NAME=M600_state

[gcode_macro Z_Offset]
gcode:
    M117 Heating bed & nozzle...
    M104 S{150}
    M190 S{params.BED_TEMP|default(60, true)}
    G28
    TEMPERATURE_WAIT SENSOR=heater_bed MINIMUM={params.BED_TEMP|default(60, true)}
    PROBE_CALIBRATE

[gcode_macro CANCEL_PRINT]
description: Cancel the actual running print
rename_existing: CANCEL_PRINT_BASE
variable_park: True
gcode:
  ## Move head and retract only if not already in the pause state and park set to true
  {% if printer.pause_resume.is_paused|lower == 'false' and park|lower == 'true'%}
    _TOOLHEAD_PARK_PAUSE_CANCEL
  {% endif %}
  TURN_OFF_HEATERS
  M106 S0
  CANCEL_PRINT_BASE
  M84



# Home, get position, throw around toolhead, home again.
# If MCU stepper positions (first line in GET_POSITION) are greater than a full step different (your number of microsteps), then skipping occured.
# We only measure to a full step to accomodate for endstop variance.
# Example: TEST_SPEED SPEED=300 ACCEL=5000 ITERATIONS=10

[gcode_macro TEST_SPEED]
gcode:
    # Speed
    {% set speed  = params.SPEED|default(printer.configfile.settings.printer.max_velocity)|int %}
    # Iterations
    {% set iterations = params.ITERATIONS|default(5)|int %}
    # Acceleration
    {% set accel  = params.ACCEL|default(printer.configfile.settings.printer.max_accel)|int %}
    # Bounding inset for large pattern (helps prevent slamming the toolhead into the sides after small skips, and helps to account for machines with imperfectly set dimensions)
    {% set bound = params.BOUND|default(20)|int %}
    # Size for small pattern box
    {% set smallpatternsize = SMALLPATTERNSIZE|default(20)|int %}
    
    # Large pattern
        # Max positions, inset by BOUND
        {% set x_min = printer.toolhead.axis_minimum.x + bound %}
        {% set x_max = printer.toolhead.axis_maximum.x - bound %}
        {% set y_min = printer.toolhead.axis_minimum.y + bound %}
        {% set y_max = printer.toolhead.axis_maximum.y - bound %}
    
    # Small pattern at center
        # Find X/Y center point
        {% set x_center = (printer.toolhead.axis_minimum.x|float + printer.toolhead.axis_maximum.x|float ) / 2 %}
        {% set y_center = (printer.toolhead.axis_minimum.y|float + printer.toolhead.axis_maximum.y|float ) / 2 %}
        
        # Set small pattern box around center point
        {% set x_center_min = x_center - (smallpatternsize/2) %}
        {% set x_center_max = x_center + (smallpatternsize/2) %}
        {% set y_center_min = y_center - (smallpatternsize/2) %}
        {% set y_center_max = y_center + (smallpatternsize/2) %}

    # Save current gcode state (absolute/relative, etc)
    SAVE_GCODE_STATE NAME=TEST_SPEED
    
    # Output parameters to g-code terminal
    { action_respond_info("TEST_SPEED: starting %d iterations at speed %d, accel %d" % (iterations, speed, accel)) }
    
    # Home and get position for comparison later:
        G28
        # QGL if not already QGLd (only if QGL section exists in config)
        {% if printer.configfile.settings.quad_gantry_level %}
            {% if printer.quad_gantry_level.applied == False %}
                QUAD_GANTRY_LEVEL
                G28 Z
            {% endif %}
        {% endif %} 
        # Move 50mm away from max position and home again (to help with hall effect endstop accuracy - https://github.com/AndrewEllis93/Print-Tuning-Guide/issues/24)
        G90
        G1 X{printer.toolhead.axis_maximum.x-50} Y{printer.toolhead.axis_maximum.y-50} F{30*60}
        G28 X Y
        G0 X{printer.toolhead.axis_maximum.x-1} Y{printer.toolhead.axis_maximum.y-1} F{30*60}
        G4 P1000 
        GET_POSITION

    # Go to starting position
    G0 X{x_min} Y{y_min} Z{bound + 10} F{speed*60}

    # Set new limits
    SET_VELOCITY_LIMIT VELOCITY={speed} ACCEL={accel} ACCEL_TO_DECEL={accel / 2}

    {% for i in range(iterations) %}
        # Large pattern
            # Diagonals
            G0 X{x_min} Y{y_min} F{speed*60}
            G0 X{x_max} Y{y_max} F{speed*60}
            G0 X{x_min} Y{y_min} F{speed*60}
            G0 X{x_max} Y{y_min} F{speed*60}
            G0 X{x_min} Y{y_max} F{speed*60}
            G0 X{x_max} Y{y_min} F{speed*60}
            
            # Box
            G0 X{x_min} Y{y_min} F{speed*60}
            G0 X{x_min} Y{y_max} F{speed*60}
            G0 X{x_max} Y{y_max} F{speed*60}
            G0 X{x_max} Y{y_min} F{speed*60}
        
        # Small pattern
            # Small diagonals 
            G0 X{x_center_min} Y{y_center_min} F{speed*60}
            G0 X{x_center_max} Y{y_center_max} F{speed*60}
            G0 X{x_center_min} Y{y_center_min} F{speed*60}
            G0 X{x_center_max} Y{y_center_min} F{speed*60}
            G0 X{x_center_min} Y{y_center_max} F{speed*60}
            G0 X{x_center_max} Y{y_center_min} F{speed*60}
            
            # Small box
            G0 X{x_center_min} Y{y_center_min} F{speed*60}
            G0 X{x_center_min} Y{y_center_max} F{speed*60}
            G0 X{x_center_max} Y{y_center_max} F{speed*60}
            G0 X{x_center_max} Y{y_center_min} F{speed*60}
    {% endfor %}

    # Restore max speed/accel/accel_to_decel to their configured values
    SET_VELOCITY_LIMIT VELOCITY={printer.configfile.settings.printer.max_velocity} ACCEL={printer.configfile.settings.printer.max_accel} ACCEL_TO_DECEL={printer.configfile.settings.printer.max_accel_to_decel} 

    # Re-home and get position again for comparison:
        G28
        # Go to XY home positions (in case your homing override leaves it elsewhere)
        G90
        G0 X{printer.toolhead.axis_maximum.x-1} Y{printer.toolhead.axis_maximum.y-1} F{30*60}
        G4 P1000 
        GET_POSITION

    # Restore previous gcode state (absolute/relative, etc)
    RESTORE_GCODE_STATE NAME=TEST_SPEED